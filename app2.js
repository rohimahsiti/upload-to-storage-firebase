import React from 'react';
import {
  ActivityIndicator,
  Button,
  Clipboard,
  Image,
  Share,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert
} from 'react-native';
import { ImagePicker, Permissions } from 'expo';
import uuid from 'uuid';
import * as firebase from 'firebase';

console.disableYellowBox = true;

const url =
  'https://firebasestorage.googleapis.com/v0/b/blobtest-36ff6.appspot.com/o/Obsidian.jar?alt=media&token=93154b97-8bd9-46e3-a51f-67be47a4628a';


export default class App extends React.Component {
  constructor(props) {
    super(props);
    state = {
      image: null,
      uploading: false,
    };
    const firebaseConfig = {
      apiKey: "AIzaSyAWD42cYoB31gGMbkb1hmOSgY2C-BE0Qq4",
      authDomain: "agrimo-75988.firebaseapp.com",
      databaseURL: "https://agrimo-75988.firebaseio.com",
      projectId: "agrimo-75988",
      storageBucket: "agrimo-75988.appspot.com",
      messagingSenderId: "1021305431082"
    };

    if(!firebase.apps.length){firebase.initializeApp(firebaseConfig);}
  }

  async componentDidMount() {
    await Permissions.askAsync(Permissions.CAMERA_ROLL);
    await Permissions.askAsync(Permissions.CAMERA);
  }

  onChooseImage = async () => {
    // let result = await ImagePicker.launchCameraAsync()
    let result = await ImagePicker.launchCameraAsync()
    console.log(result)
    if (!result.cancelled) {
      this.uploadImage(result.uri, "test-image")
        .then(() => {
          Alert.alert("success")
        })
        .catch((error) => {
          console.log(error)
          Alert.alert("failed")
        })
    }
  }

  uploadImage = async (uri, imageName) => {
    const response = await fetch(uri);
    const blob = await response.blob()

    var ref = firebase.storage().ref().child("images/" + imageName)
    return ref.put(blob)
  }

  render() {
    return (
      <View style={styles.container}>
        <Button title="Choose image..." onPress={this.onChooseImage} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 50,
    alignItems: 'center',
  }
})